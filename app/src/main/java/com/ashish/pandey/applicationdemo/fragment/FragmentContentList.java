package com.ashish.pandey.applicationdemo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ashish.pandey.applicationdemo.AppController;
import com.ashish.pandey.applicationdemo.MainActivity;
import com.ashish.pandey.applicationdemo.R;
import com.ashish.pandey.applicationdemo.adapter.AdapterContentListData;
import com.ashish.pandey.applicationdemo.model.Hits;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by IXEET pc9 on 3/30/2017.
 */

public class FragmentContentList extends Fragment {
    Context mContext;
    private View view;

    private ArrayList<Hits> listHits;
    private RecyclerView rvContentList;
    ProgressBar pbLoading;
    private int pos;
    private FetchUrl fetchUrl;
    private String url = "https://pixabay.com/api/?key=4909307-b42f8c86ca7153ec47ea96145&q=yellow+flowers&image_type=photo&per_page=20&page=";
    private int offset = 1;
    private SwipeRefreshLayout srContentList;
    private boolean mIsLoading = false, swipeRefreshData = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if ((fetchUrl != null) && (fetchUrl.getStatus() == AsyncTask.Status.RUNNING)) {
            fetchUrl.cancel(true);
        }
        mContext = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // if (view == null) {
        view = inflater.inflate(R.layout.fragment_content_list, container, false);
        initView(view);
        initListener();
        listHits = new ArrayList<>();
        setAdapterForRecyclerView();
        //  }
        return view;
    }


    private void initListener() {
        srContentList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshData = true;
                pbLoading.setVisibility(View.VISIBLE);
                listHits.clear();
                mIsLoading = true;
                offset = 0;
                loadContentList();
                //loadResponseJson();
            }
        });

        rvContentList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (!mIsLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        mIsLoading = true;
                        loadContentList();
                    }
                }
            }
        });
    }

    private void initView(View view) {
        pbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        rvContentList = (RecyclerView) view.findViewById(R.id.rv_content_list);
        srContentList = (SwipeRefreshLayout) view.findViewById(R.id.sr_content_list);

    }

    private void loadContentList() {
        if (AppController.isConnected) {
            if (offset <= 20) {
                fetchUrl = new FetchUrl();
                srContentList.setRefreshing(true);
                // Start downloading json data from Google Directions API
                fetchUrl.execute(url + offset);
            }
        } else {
            Toast.makeText(mContext, "Please connect to internet", Toast.LENGTH_SHORT).show();
        }
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {


        @Override
        protected void onCancelled() {
            //super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            pbLoading.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                //Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                //  Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (!TextUtils.isEmpty(result)) {
                parseObject(result);
                offset++;
                rvContentList.getAdapter().notifyDataSetChanged();
                mIsLoading = false;
                srContentList.setRefreshing(false);
                pbLoading.setVisibility(View.GONE);

            } else {
                Toast.makeText(mContext, "loading api failed", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void parseObject(String result) {
        //ContentListModel contentListModel = new ContentListModel();
        try {
            JSONObject jObject = new JSONObject(result);
            JSONArray hitsJsonArray = jObject.getJSONArray("hits");
            for (int i = 0; i < hitsJsonArray.length(); i++) {
                JSONObject object = (JSONObject) hitsJsonArray.get(i);
                Hits hits = new Hits();
                hits.setUserImageURL((String) object.get("userImageURL"));
                hits.setTags((String) object.get("tags"));
                listHits.add(hits);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(15000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setDoInput(true);
            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            //Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            // Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            listHits.addAll((ArrayList<Hits>) savedInstanceState.getSerializable("contentList"));
            pos = savedInstanceState.getInt("pos");
            offset = savedInstanceState.getInt("offset");
        }
        if (listHits != null && listHits.size() > 0) {
            if (pos > 0) {
                rvContentList.scrollToPosition(pos);
            }
        } else {
            loadContentList();
        }
    }

    private void setAdapterForRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvContentList.setLayoutManager(new LinearLayoutManager(mContext));
        AdapterContentListData adapterContentListData = new AdapterContentListData(mContext, listHits);
        rvContentList.setAdapter(adapterContentListData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("contentList", listHits);
        outState.putInt("pos", ((LinearLayoutManager) rvContentList.getLayoutManager()).findFirstVisibleItemPosition());
        outState.putInt("offset", offset);
    }
}
