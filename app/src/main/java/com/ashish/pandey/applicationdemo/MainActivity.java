package com.ashish.pandey.applicationdemo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ashish.pandey.applicationdemo.Utility.Utils;
import com.ashish.pandey.applicationdemo.adapter.AdapterContentListData;
import com.ashish.pandey.applicationdemo.fragment.FragmentContentDetail;
import com.ashish.pandey.applicationdemo.fragment.FragmentContentList;

public class MainActivity extends AppCompatActivity implements AdapterContentListData.OnContentClick {

    private boolean storagePermission;
    public static int PERMISSIONS_REQUEST_STORAGE = 13;
    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkStoragePermission();
        if (savedInstanceState != null) {
            if (storagePermission) {
                //loadContentList();
                // cleanup any existing fragments in case we are in detailed mode
                /*getFragmentManager().executePendingTransactions();
                Fragment fragmentById = getFragmentManager().
                        findFragmentById(R.id.fl_main);
                if (fragmentById != null) {
                    getFragmentManager().beginTransaction()
                            .remove(fragmentById).commit();
                }
                loadContentListFragment();*/
            } else {
                Utils.showPermissionDialog(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_STORAGE);
            }
        } else {
            if (storagePermission) {
                loadContentListFragment();
            } else {
                Utils.showPermissionDialog(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSIONS_REQUEST_STORAGE);
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_STORAGE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Displaying a toast
                Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
                storagePermission = true;
                loadContentListFragment();

            } else {
                //Displaying another toast if permission is not granted
                storagePermission = false;
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Utils.isPermissionAllowed(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                storagePermission = false;
                return;
            } else {
                storagePermission = true;
                return;
            }
        }
        storagePermission = true;
    }

    @Override
    public void onContentClicked(String url) {
        loadContentDetailFragment(url);
    }

    private void loadContentDetailFragment(String url) {
        FragmentContentDetail fragmentContentDetail = new FragmentContentDetail();
        Bundle bundle = new Bundle();
        bundle.putString("userImageURL", url);
        fragmentContentDetail.setArguments(bundle);
        if (!isFinishing()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fl_main, fragmentContentDetail).addToBackStack(String.valueOf(++counter)).commit();
        }
    }


    private void loadContentListFragment() {
        FragmentContentList fragmentContentList = new FragmentContentList();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fl_main, fragmentContentList).addToBackStack(String.valueOf(++counter)).commit();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
            return;
        } else {
            getSupportFragmentManager().popBackStack(String.valueOf(counter--), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}
