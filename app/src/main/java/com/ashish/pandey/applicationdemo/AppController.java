package com.ashish.pandey.applicationdemo;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ashish.pandey.applicationdemo.receiver.InternetReceiver;

/**
 * Created by IXEET pc9 on 3/30/2017.
 */

public class AppController extends Application {
    public static boolean isConnected = false;

    @Override
    public void onCreate() {
        super.onCreate();
        setInternetStatus();
        registerNetworkStatus();
    }

    private void setInternetStatus() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        AppController.isConnected = isConnected;
    }

    private void registerNetworkStatus() {
        BroadcastReceiver receiver = new InternetReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getApplicationContext().registerReceiver(receiver, filter);
    }
}
