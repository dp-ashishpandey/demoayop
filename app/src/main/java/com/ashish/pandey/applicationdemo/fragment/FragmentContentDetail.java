package com.ashish.pandey.applicationdemo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ashish.pandey.applicationdemo.R;
import com.ashish.pandey.applicationdemo.Utility.ImageLoader;

import java.util.ArrayList;

/**
 * Created by IXEET pc9 on 3/30/2017.
 */

public class FragmentContentDetail extends Fragment {
    Context mContext;
    private View view;
    private String url;

    private ImageView ivContentDetail;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //if (view == null) {
        view = inflater.inflate(R.layout.fragment_content_detail, container, false);
        initView(view);
        // }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            url = savedInstanceState.getString("userImageURL");
        } else {
            url = getArguments().getString("userImageURL");
        }
        if (!TextUtils.isEmpty(url)) {
            ImageLoader imageLoader = new ImageLoader(mContext);
            imageLoader.DisplayImage(url, R.drawable.loader, ivContentDetail);
        }
    }

    private void initView(View view) {

        ivContentDetail = (ImageView) view.findViewById(R.id.iv_content_detail);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("userImageURL", url);
    }
}
