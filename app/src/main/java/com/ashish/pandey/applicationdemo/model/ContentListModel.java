package com.ashish.pandey.applicationdemo.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IXEET pc9 on 3/30/2017.
 */

public class ContentListModel implements Serializable {
    private String total;

    private ArrayList<Hits> hits;

    private String totalHits;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<Hits> getHits() {
        return hits;
    }

    public void setHits(ArrayList<Hits> hits) {
        this.hits = hits;
    }

    public String getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(String totalHits) {
        this.totalHits = totalHits;
    }

    @Override
    public String toString() {
        return "ClassPojo [total = " + total + ", hits = " + hits + ", totalHits = " + totalHits + "]";
    }
}
