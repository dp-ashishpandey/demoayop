package com.ashish.pandey.applicationdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ashish.pandey.applicationdemo.R;
import com.ashish.pandey.applicationdemo.Utility.ImageLoader;
import com.ashish.pandey.applicationdemo.model.Hits;

import java.util.ArrayList;

/**
 * Created by IXEET pc9 on 2/2/2017.
 */

public class AdapterContentListData extends RecyclerView.Adapter<AdapterContentListData.MyViewHolder> {

    private Context mContext;
    ArrayList<Hits> listHits;
    ImageLoader imgLoader;
    OnContentClick onContentClick;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvContentTag;
        private ImageView ivContent;

        public MyViewHolder(View view) {
            super(view);
            ivContent = (ImageView) view.findViewById(R.id.iv_content);
            tvContentTag = (TextView) view.findViewById(R.id.tv_content_tag);

        }
    }


    public AdapterContentListData(Context mContext, ArrayList<Hits> listHits) {
        this.mContext = mContext;
        this.listHits = listHits;
        onContentClick = (OnContentClick) mContext;
        imgLoader = new ImageLoader(mContext.getApplicationContext());
    }

    @Override
    public AdapterContentListData.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_content_list, parent, false);

        return new AdapterContentListData.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AdapterContentListData.MyViewHolder holder, final int position) {
        final Hits hitsData = listHits.get(position);
        holder.tvContentTag.setText(hitsData.getTags());

        imgLoader.DisplayImage(hitsData.getUserImageURL(), R.drawable.loader, holder.ivContent);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onContentClick.onContentClicked(hitsData.getUserImageURL());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listHits != null ? listHits.size() : 0;
    }

    public interface OnContentClick {
        void onContentClicked(String url);
    }
}


