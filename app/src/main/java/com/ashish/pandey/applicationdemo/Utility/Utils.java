package com.ashish.pandey.applicationdemo.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by IXEET pc9 on 3/30/2017.
 */
public class Utils {
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static boolean isPermissionAllowed(Context mContext, String permission) {
        int result = ContextCompat.checkSelfPermission(mContext, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;
        //If permission is not granted returning false
        return false;
    }

    public static void showPermissionDialog(Activity activity, String permission, int requestPermissionId) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(activity, new String[]{permission}, requestPermissionId);
    }
}
